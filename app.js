const express = require('express'); /*Require do pacote express*/
const app = express();/*Criação do servidor */ 

app.use(express.json()) /*Utlizando o pacote JSON, atraves do EXPRESS*/


/* REQ = REQUISIÇÃO / RES = RESPOSTA */

app.get('/',(req,res) => {
    res.json({
        chave : "Olá"
    })
})

app.post('/',(req,res) => {
     res.json(req.body) 
})

app.put('/:nome/:qualquer',(req,res) => {
     res.json(req.params) 
})

app.delete('/',(req,res) => {
     res.json(req.query) 
})


const port = 8000;
app.listen(port, ()=>{
    console.log("===========================")
    console.log(`Rodando na porta ${port}`)
});
